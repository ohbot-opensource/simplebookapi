const axios = require('axios')

/**
 * Post
 * @param requestOption
 * @param url
 * @param method
 * @param postData
 * @returns {Promise<any>}
 */
function post(requestOption, url, method, postData) {
    return new Promise((resolve, reject) => {
        axios.post( url,
                {
                    jsonrpc: '2,0',
                    method: method,
                    params: postData,
                    id: 1,
                },
                {
                    headers: requestOption
                }
            )
            .then(response => {
                console.log(response.data)
                if (response.data.error) {
                    console.log(response.data.error)
                    reject({
                        success: false,
                        data: response.data.error
                    })
                } else if (response.data.result.errors) {
                    console.log(response.data.result.errors)
                    reject({
                        success: false,
                        data: response.data.result.errors
                    })
                } else if (response.data.result) {
                    console.log(response.data.result)
                    resolve({
                        success: true,
                        data: response.data.result
                    })
                }
            })
            .catch(err => {
                reject({
                    success: false,
                    data: err
                })
            })
    })
}

module.exports = {
    post: post
}
