
module.exports = class BookFilter {

    constructor(date_from){
        /**
         * 起始日期 ==> Format : 'Y-m-d'
         */
        this.date_from = date_from

        /**
         * 起始時間 ==> Format : 'H:i:s'
         */
        this.time_from = ''

        /**
         * 終止日期 ==> Format : 'Y-m-d'
         */
        this.date_to = ''

        /**
         * 起始時間 ==> Format : 'H:i:s'
         */
        this.time_to = ''

        /**
         * 創建起始日期 ==> Format : 'Y-m-d'
         */
        this.created_date_from = ''

        /**
         * 創建終止日期 ==> Format : 'Y-m-d'
         */
        this.created_date_to = ''

        /**
         * 員工Group Id ==> Integer
         */
        this.unit_group_id = ''

        /**
         * 服務ID ==> Integer
         */
        this.event_id = ''

        /**
         * 是否為最新的訂單 ==> Type : 1 or 0
         */
        this.is_confirmed = ''

        /**
         * 顧客ID ==> Integer
         */
        this.client_id = ''

        /**
         * 排列順序 ==> Type : date_start、record_date、date_start_asc
         */
        this.order = ''

        /**
         * 狀態選擇
         *   ==> plugin not active : Type : all、cancelled、non_cancelled
         *   ==> plugin active : Type : all、cancelled、non_cancelled、cancelled_by_client...
         */
        this.booking_type = ''

        /**
         * 訂單編號
         */
        this.code = ''
    }
}
