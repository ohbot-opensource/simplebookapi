
module.exports.ServiceProvider = class ServiceProvider {
    constructor(providerName, description){
        /**
         * 服務提供者的姓名
         */
        this.name = providerName

        /**
         * 服務提供者的敘述
         */
        this.description = description

        /**
         * 服務提供者的電話
         */
        this.phone = ''

        /**
         * 服務提供者的Email
         */
        this.email = ''

        /**
         * 服務提供者在當前時段內可以提供的次數
         */
        this.qty = '1'

        /**
         * 是否顯示
         */
        this.is_visible = '1'

        /**
         * 是否使用API
         */
        this.is_active = '1'

        /**
         * 服務提供者的順位
         */
        this.position = '0'

        /**
         * 服務提供者的順位
         */
        this.position_in_location = null

        /**
         * 服務提供者的順位
         */
        this.locations = null

        /**
         * 服務提供者的Service
         */
        this.services = []
    }
}

module.exports.ServiceProviderLocation = class ServiceProviderLocation{

    constructor(title, description){
        /**
         * 服務地區名稱
         */
        this.title = title

        /**
         * 服務地區的敘述
         */
        this.description = description

        /**
         * 服務地區的地址1
         */
        this.address1 = ''

        /**
         * 服務地區的地址2
         */
        this.address2 = ''

        /**
         * 服務地區的城市
         */
        this.city = ''

        /**
         * 服務地區的郵遞區號
         */
        this.zip = ''

        /**
         * 服務地區的城市ID
         */
        this.country_id = '0'

        /**
         * 服務地區的經度
         */
        this.lat = '0.00000000000000'

        /**
         * 服務地區的緯度
         */
        this.lng = '0.00000000000000'

        /**
         * 服務地區的電話
         */
        this.phone = ''

        /**
         * 服務地區的順位
         */
        this.position = '0'

        /**
         * 是否為預設的服務地區
         */
        this.is_default = '0'
    }
}
