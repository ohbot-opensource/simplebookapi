
module.exports.Service = class Service {
    constructor(serviceName, description){
        /**
         * 服務名稱
         */
        this.name = serviceName

        /**
         * 服務敘述
         */
        this.description = description

        /**
         * 每次服務長度（分鐘）
         */
        this.duration = '60'

        /**
         * 隱藏預約頁面上的時間長度
         */
        this.hide_duration = '0'

        /**
         * 是否為公開的服務
         */
        this.is_public = '1'

        /**
         * 是否被使用
         */
        this.is_active = '1'

        /**
         * 服務的排序
         */
        this.position = '0'

        /**
         *
         */
        this.file_id = null

        /**
         *
         */
        this.seo_url = null

        /**
         * 是否為重複性方案
         */
        this.is_recurring = '0'

        /**
         * 服務的圖片
         */
        this.picture = null

        /**
         * 服務的圖片子路徑
         */
        this.picture_sub_path = null

        /**
         * 服務的圖片路徑
         */
        this.picture_path = null

        /**
         * 重複性方案的設定
         * {
         *       "days":7,
         *       "repeat_count":1,
         *       "type":"fixed",
         *       "mode":"skip",
         *       "is_default_settings":true,
         *       "days_names":[
         *          "Friday",
         *          "Saturday",
         *          "Sunday"
         *       ]
         * },
         */
        this.recurring_settings = null

        /**
         * 服務提供者的ID
         */
        this.units = []

        /**
         * 服務提供者的類別
         */
        this.categories = null

        /**
         * 服務的價格
         */
        this.price = 0

        /**
         * 服務的幣別
         */
        this.currency = 'TWD'
    }
}

module.exports.WorkDayInfo = class WorkDayInfo {
    constructor(startTime, endTime) {
        /**
         * 起始時間
         */
        this.start_time = startTime

        /**
         * 最後時間
         */
        this.end_time = endTime

        /**
         * 是否有開服務
         */
        this.is_day_off = 0

        /**
         * 休息時間: Array, {"start_time":"14:00","end_time":"15:00"}
         */
        this.breaktime = []

        /**
         * 星期: 1-7 for Monday - Sunday
         */
        this.index = ""

        /**
         * 時間的名稱
         */
        this.name = ""

        /**
         * date is used to set worktime for special date
         */
        this.date = ""

        /**
         * 服務提供者Id
         */
        this.unit_group_id = ""

        /**
         * 服務Id
         */
        this.event_id = ""
    }
}
