const BaseService = require('./base.service')
const http = require('../lib/https')
const md5 = require('md5');

module.exports = class PublicService extends BaseService{

    /**
     * 建構子
     * @param config
     * @param token
     */
    constructor(config, token){
        let option = {
            'X-Company-Login': config.CompanyName,
            'X-Token': token,
            'Content-Type': 'application/json',
        }

        super(config.CompanyName, '', option)

        /**
         * 儲存secretKey
         */
        this.secretKey = config.SecretKey
    }
    // ================================
    //           General API
    // ================================

    /**
     * 取得國家列表
     */
    getCountryList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getCountryList",
            postData)
    }

    /**
     * 取得服務列表
     */
    getEventList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getEventList",
            postData)
    }

    /**
     * 取得服務提供者列表
     */
    getUnitList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getUnitList",
            postData)
    }

    /**
     * 取得類別列表
     */
    getCategoriesList() {
        let postData = []

        return http.post(this.RequestOption,
            this.BaseURL,
            "getCategoriesList",
            postData)
    }

    /**
     * 取得服務的第一天工作日
     */
    getFirstWorkingDay(eventId) {
        let postData = this.setArray(eventId)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getFirstWorkingDay",
            postData)
    }

    /**
     * 取得服務的營業時間
     */
    getWorkCalendar(year, month, eventId, unitGroupId = 0) {
        let group = {
            "unit_group_id": unitGroupId,
            "event_id": eventId
        }

        let postData = this.setArray(year, month, group)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getWorkCalendar",
            postData)
    }

    /**
     * 取得服務的工作區間：{"09:00-10:00", "10:00-11:00", ...}
     */
    getStartTimeMatrix(startDate, endDate, eventId, unitId, count = 1) {
        let postData = this.setArray(startDate, endDate, eventId, unitId, count)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getStartTimeMatrix",
            postData)
    }

    /**
     * 由起始時間抓出結束時間
     */
    calculateEndTime(startDateTime, eventId, unitId) {
        let postData = this.setArray(startDateTime, eventId, unitId)

        return http.post(this.RequestOption,
            this.BaseURL,
            "calculateEndTime",
            postData)
    }

    /**
     *
     */
    getServiceAvailableTimeIntervals(startDate, endDate, eventId, unitId, count = 1) {
        let postData = this.setArray(startDate, endDate, eventId, unitId, count)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getServiceAvailableTimeIntervals",
            postData)
    }

    // ================================
    //           Book API
    // ================================
    /**
     * 訂購服務
     */
    book(eventId, unitId, date, time, clientData, count = 1) {
        let postData = this.setArray(eventId, unitId, date, time, clientData, {}, count)

        return http.post(this.RequestOption,
            this.BaseURL,
            "book",
            postData)
    }

    /**
     * 取得訂購服務
     */
    getBooking(bookingId, bookingHash) {
        let sign = md5(bookingId + bookingHash + this.secretKey)

        let postData = this.setArray(bookingId, sign)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getBooking",
            postData)
    }

    // ================================
    //           Client API
    // ================================
    /**
     * 取得使用者資訊
     */
    getClientInfo(clientId, clientHash) {
        let sign = md5(clientId + clientHash + this.goldenKey)

        let postData = this.setArray(clientId, sign)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getClientInfo",
            postData)
    }

    /**
     * 取得使用者資訊by帳號密碼
     */
    getClientInfoByLoginPassword(login, password) {
        let postData = this.setArray(login, password)

        return http.post(this.RequestOption,
            this.BaseURL,
            "getClientInfoByLoginPassword",
            postData)
    }
}
