## Installing

Using npm :

<pre>
$ npm install simplybook-js-api
</pre>

## Example
**Public Service**

* 取得Token

<pre>
    const SimplyBook = require("simplybook-js-api");
    
    const simplyBook = new SimplyBook(
        YOUR_COMPANY_NAME,
        YOUR_APIKEY,
        YOUR_SECRET_KEY,
        YOUR_ADMIN,
        YOUR_ADMIN_PASSWORD)
        
    // 建立Auth Service
    let auth = simplyBook.createAuthService()
    
    // 取得Token
    let token = await this.auth.getToken()
    
    // cabe12dac8ba2e4aa2fbdcf16021f55b0ce673c3123bfb5ebd9ac608231373ecf
    console.lot(token.data)
</pre>

* 取得Public Service相關資源

<pre>
    // 建立Public Service
    let publicService = simplyBook.createPublicService(token.data)
    
    // 取得Event List
    let event = await publicService.getEventList()
    
    let eventList = Object.values(event.data)
</pre>

* 取得Admin Service相關資源

<pre>
    // 建立Admin Service
    let adminService = simplyBook.createAdminService(token.data)
    
    // 取得Event List
    let event = await adminService.getEventList()
    
    let eventList = Object.values(event.data)
</pre>

* Model的使用（新增一項服務）

<pre>
    // 建立Service Model （建構子帶入服務名稱、服務的敘述）
    let serviceModel = simplyBook.createServiceModel('美髮服務', '提供您美髮、按摩服務')
    
    // ---- 設定 properties
    // 服務時間長度
    serviceModel.duration = 60
    
    // 提供服務的價格
    serviceModel.price = 100
    
    // 幣種（可由getCompanyCurrency取得）
    serviceModel.currency = 'TWD'
    
    // 是否顯示
    serviceModel.is_public = "1"
    
    // 服務供應者的ID
    serviceModel.units = [2, 3]
    
    // 新增一項服務
    adminService.addService(serviceModel)
        .then(data => {
            window.alert("新增成功");
        })
        .catch(err => {
            window.alert("新增失敗");
        })
</pre>

* Model的使用（新增服務提供者）

<pre>
    // 建立Provider Model （建構子帶入服務提供者姓名、服務提供者的敘述）
    let providerModel = simplyBook.createProviderModel('王美美', '專業美髮師')
    
    // ---- 設定 properties
    // 服務提供者電子信箱
    providerModel.email = 'test@test.com'
    
    // 服務提供者電話
    providerModel.phone = '09xxxxxxxx'
    
    // 是否顯示
    providerModel.is_visible = "1"
    
    // 同一個時間可以服務多少人
    providerModel.qty = 5
    
    // 服務提供者提供的服務項目ID
    providerModel.services = [1, 8]
    
    // 新增服務提供者
    this.admin.addServiceProvider(providerModel)
        .then(data => {
            window.alert("新增成功");
        })
        .catch(err => {
            window.alert("新增失敗");
        })
</pre>
